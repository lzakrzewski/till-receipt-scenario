# till-receipt-scenario [![Build Status](https://travis-ci.org/lzakrzewski/till-receipt-scenario.svg?branch=master)](https://travis-ci.org/lzakrzewski/till-receipt-scenario)
CLI system in PHP that can be used for a till receipt to calculate and display all products with a subtotal and a grand total.

## Requirements
```json
  "require": {
    "php": ">=7.0",
    "beberlei/assert": "~2.6",
    "symfony/console": "~3.0"
  },
```

## Installation
Clone repository
```sh
git clone git@github.com:lzakrzewski/till-receipt-scenario.git
```

Go to repository folder and then install composer dependencies
```sh
cd till-receipt-scenario && composer install
```

## Testing
Here are a few commands which helps to test system
- `composer spec` (`PHPSpec` was used for unit test of domain model)
- `composer integration` (`PHPUnit` was used for test integration with `symfony/console`)
- `composer static-analysis` (`php-cs-fixer` was used for static analysis)

## Usage
CLI works in interactive mode

#### Example
```sh
bin/console till-receipt:scenario
```

#### Example output
![](resources/example-output.png)