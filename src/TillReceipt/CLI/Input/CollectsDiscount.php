<?php

declare(strict_types=1);

namespace TillReceipt\CLI\Input;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use TillReceipt\Model\Price;

class CollectsDiscount
{
    /** @var QuestionHelper */
    private $questionHelper;

    public function __construct(QuestionHelper $questionHelper)
    {
        $this->questionHelper = $questionHelper;
    }

    public function collect(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('');

        $confirmation = new ConfirmationQuestion('Would you like to apply discount? ', false);

        if (!$this->questionHelper->ask($input, $output, $confirmation)) {
            return;
        }

        return Price::fromStringValues(
            $this->askQuestion('Please enter the discount <comment>price</comment>: ', $input, $output),
            $this->askQuestion('Please enter the discount <comment>currency</comment>: ', $input, $output)
        );
    }

    private function askQuestion(string $question, InputInterface $input, OutputInterface $output): string
    {
        $question = new Question($question, false);

        return (string) $this->questionHelper->ask($input, $output, $question);
    }
}
