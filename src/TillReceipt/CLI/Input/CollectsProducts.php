<?php

declare(strict_types=1);

namespace TillReceipt\CLI\Input;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use TillReceipt\Model\Price;
use TillReceipt\Model\Product;

class CollectsProducts
{
    /** @var QuestionHelper */
    private $questionHelper;

    public function __construct(QuestionHelper $questionHelper)
    {
        $this->questionHelper = $questionHelper;
    }

    public function collect(InputInterface $input, OutputInterface $output): array
    {
        $output->writeln('');

        $products = [];

        while (true) {
            $products[] = Product::enter(
                $this->askQuestion('Please enter the <comment>name</comment> of product: ', $input, $output),
                Price::fromStringValues(
                    $this->askQuestion('Please enter the <comment>price</comment> of this product: ', $input, $output),
                    $this->askQuestion('Please enter the <comment>currency</comment> of this product: ', $input, $output)
                )
            );

            $output->writeln('');

            $confirmation = new ConfirmationQuestion('Continue with adding products? ', false);

            if (!$this->questionHelper->ask($input, $output, $confirmation)) {
                break;
            }
        }

        return $products;
    }

    private function askQuestion(string $question, InputInterface $input, OutputInterface $output): string
    {
        $question = new Question($question, false);

        return (string) $this->questionHelper->ask($input, $output, $question);
    }
}
