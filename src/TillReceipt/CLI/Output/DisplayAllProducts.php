<?php

declare(strict_types=1);

namespace TillReceipt\CLI\Output;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\OutputInterface;
use TillReceipt\Model\TillReceipt;

class DisplayAllProducts
{
    public function display(TillReceipt $tillReceipt, OutputInterface $output)
    {
        $output->writeln('');

        $table = new Table($output);
        $table->setHeaders(['product', 'price']);

        foreach ($tillReceipt->products() as $product) {
            $table->addRow([$product->name(), (string) $product->price()]);
        }

        $table->addRow(new TableSeparator());
        $table->addRow(['Sub-total', (string) $tillReceipt->subTotal()]);
        if ($tillReceipt->hasDiscountApplied()) {
            $table->addRow(['Discounts', (string) $tillReceipt->discount()]);
        }

        $table->addRow(new TableSeparator());
        $table->addRow(['Grand-total', (string) $tillReceipt->grandTotal()]);

        $table->render();
    }
}
