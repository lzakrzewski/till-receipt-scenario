<?php

declare(strict_types=1);

namespace TillReceipt\CLI;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TillReceipt\CLI\Input\CollectsDiscount;
use TillReceipt\CLI\Input\CollectsProducts;
use TillReceipt\CLI\Output\DisplayAllProducts;
use TillReceipt\Model\Price;
use TillReceipt\Model\TillReceipt;

class TillReceiptCommand extends Command
{
    /** @var CollectsProducts */
    private $collectsProducts;

    /** @var CollectsDiscount */
    private $collectsDiscount;

    /** @var DisplayAllProducts */
    private $displayAllProducts;

    public function __construct(CollectsProducts $collectsProducts, CollectsDiscount $collectsDiscount, DisplayAllProducts $displayAllProducts)
    {
        $this->collectsProducts   = $collectsProducts;
        $this->collectsDiscount   = $collectsDiscount;
        $this->displayAllProducts = $displayAllProducts;

        parent::__construct('till-receipt:scenario');
    }

    protected function configure()
    {
        $this
            ->setDescription('Till receipt scenario')
            ->setHelp('CLI that can be used for a till receipt to calculate and display all products with a subtotal and a grand total.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->displayWelcomeMessage($output);

        try {
            $tillReceipt = TillReceipt::calculate($this->collectsProducts->collect($input, $output));
            $discount    = $this->collectsDiscount->collect($input, $output);

            if ($discount instanceof Price) {
                $tillReceipt->applyDiscount($discount);
            }
        } catch (\InvalidArgumentException $e) {
            $output->writeln(sprintf('<error>Error: %s</error>', $e->getMessage()));

            return 1;
        }

        $this->displayAllProducts->display($tillReceipt, $output);

        return 0;
    }

    private function displayWelcomeMessage(OutputInterface $output)
    {
        $output->writeln([
            '<info>Till receipt scenario.</info> CLI system in PHP that can be used for a till receipt to calculate',
            'and display all products with a subtotal and a grand total.',
        ]);
    }
}
