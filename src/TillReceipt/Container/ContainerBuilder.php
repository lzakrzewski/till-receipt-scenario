<?php

declare(strict_types=1);

namespace TillReceipt\Container;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\QuestionHelper;
use TillReceipt\CLI\Input\CollectsDiscount;
use TillReceipt\CLI\Input\CollectsProducts;
use TillReceipt\CLI\Output\DisplayAllProducts;
use TillReceipt\CLI\TillReceiptCommand;

final class ContainerBuilder
{
    private function __construct()
    {
    }

    public static function build(): array
    {
        $self = new self();

        return [
            QuestionHelper::class     => $helper = $self->questionHelper(),
            CollectsDiscount::class   => $collectsDiscount = $self->collectsDiscount($helper),
            CollectsProducts::class   => $collectsProducts = $self->collectsProducts($helper),
            DisplayAllProducts::class => $displayAllProducts = $self->displayAllProducts(),
            TillReceiptCommand::class => $tillReceiptCommand = $self->tillReceiptCommand(
                $collectsProducts,
                $collectsDiscount,
                $displayAllProducts
            ),
            Application::class => $self->application($tillReceiptCommand),
        ];
    }

    private function questionHelper(): QuestionHelper
    {
        return new QuestionHelper();
    }

    private function collectsDiscount(QuestionHelper $helper): CollectsDiscount
    {
        return new CollectsDiscount($helper);
    }

    private function collectsProducts(QuestionHelper $helper): CollectsProducts
    {
        return new CollectsProducts($helper);
    }

    private function displayAllProducts(): DisplayAllProducts
    {
        return new DisplayAllProducts();
    }

    private function tillReceiptCommand(
        CollectsProducts $collectsProducts,
        CollectsDiscount $collectsDiscount,
        DisplayAllProducts $displayAllProducts
    ): TillReceiptCommand {
        return new TillReceiptCommand($collectsProducts, $collectsDiscount, $displayAllProducts);
    }

    private function application(TillReceiptCommand $tillReceiptCommand): Application
    {
        $application = new Application();
        $application->add($tillReceiptCommand);

        return $application;
    }
}
