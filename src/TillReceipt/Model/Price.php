<?php

declare(strict_types=1);

namespace TillReceipt\Model;

use Assert\Assertion;

class Price
{
    /** @var int */
    private $amount;

    /** @var string */
    private $currency;

    public function __construct(int $amount, string $currency)
    {
        Assertion::greaterThan($amount, 0, sprintf('Amount of price should be positive, but "%d" given', $amount));
        Assertion::notBlank($currency, 'Currency of price should not be blank');

        $this->amount   = $amount;
        $this->currency = $currency;
    }

    public static function fromStringValues(string $stringAmount, string $currency): self
    {
        $amount = self::parseAmount($stringAmount);

        return new self($amount, trim($currency));
    }

    private static function parseAmount(string $stringAmount): int
    {
        $floatAmountString = filter_var(trim($stringAmount), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        if (false === $floatAmountString || 0 === strlen($floatAmountString)) {
            throw new \InvalidArgumentException(sprintf('Amount "%s" can not be parsed', $stringAmount));
        }

        return (int) ($floatAmountString * 100);
    }

    public function add(Price $price): Price
    {
        $this->validateSameCurrency($price);

        return new self($this->amount + $price->amount, $this->currency);
    }

    public function subtract(Price $price): Price
    {
        $this->validateSameCurrency($price);

        return new self($this->amount - $price->amount, $this->currency);
    }

    public function amount(): int
    {
        return $this->amount;
    }

    public function currency(): string
    {
        return $this->currency;
    }

    private function validateSameCurrency(Price $price)
    {
        if ($this->currency == $price->currency) {
            return;
        }

        throw new \InvalidArgumentException('Each currencies should be equal.');
    }

    public function __toString(): string
    {
        return sprintf('%.2f %s', $this->amount / 100, $this->currency());
    }
}
