<?php

declare(strict_types=1);

namespace TillReceipt\Model;

use Assert\Assertion;

final class Product
{
    /** @var string */
    private $name;

    /** @var Price */
    private $price;

    private function __construct(string $name, Price $price)
    {
        Assertion::notBlank($name);

        $this->name  = $name;
        $this->price = $price;
    }

    public static function enter(string $name, Price $price): self
    {
        return new self($name, $price);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): Price
    {
        return $this->price;
    }
}
