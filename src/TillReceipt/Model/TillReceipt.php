<?php

declare(strict_types=1);

namespace TillReceipt\Model;

use Assert\Assertion;

final class TillReceipt
{
    /** @var Product[] */
    private $products;

    /** @var Price */
    private $subTotal;

    /** @var Price */
    private $grandTotal;

    /** @var Price|null */
    private $discount;

    private function __construct(array $products)
    {
        $this->validateProducts($products);

        $this->products   = $products;
        $this->subTotal   = $subTotal   = $this->calculateSubTotal($products);
        $this->grandTotal = $subTotal;
    }

    public static function calculate(array $products): self
    {
        return new self($products);
    }

    public function applyDiscount(Price $discount)
    {
        if ($this->subTotal->amount() <= $discount->amount()) {
            throw new \InvalidArgumentException('Discount should not be grater than sub-total.');
        }

        $this->grandTotal = $this->calculateGrandTotal($this->subTotal, $discount);
        $this->discount   = $discount;
    }

    public function products(): array
    {
        return $this->products;
    }

    public function subTotal(): Price
    {
        return $this->subTotal;
    }

    public function grandTotal(): Price
    {
        return $this->grandTotal;
    }

    public function discount()
    {
        return $this->discount;
    }

    public function hasDiscountApplied(): bool
    {
        return $this->discount !== null;
    }

    private function validateProducts(array $products)
    {
        $count = count($products);

        Assertion::greaterOrEqualThan($count, 1);
        Assertion::allIsInstanceOf($products, Product::class);
    }

    private function calculateGrandTotal(Price $subTotal, Price $discount): Price
    {
        return $subTotal->subtract($discount);
    }

    private function calculateSubTotal(array $products): Price
    {
        return array_reduce($products, function ($carry, Product $product) {
            if (null === $carry) {
                return $product->price();
            }

            return $product->price()->add($carry);
        });
    }
}
