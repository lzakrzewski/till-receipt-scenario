<?php

declare(strict_types=1);

namespace tests\integration;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use TillReceipt\Container\ContainerBuilder;

abstract class IntegrationTestCase extends TestCase
{
    /** @var array */
    private $container;

    /** @var CommandTester */
    private $tester;

    protected function container(): array
    {
        return $this->container;
    }

    protected function executeCLI(Command $cli, array $inputs = [])
    {
        $this->tester = new CommandTester($cli);
        $this->tester->setInputs($inputs);
        $this->tester->execute([]);
    }

    protected function assertExitCode(int $expectedStatus)
    {
        $this->assertEquals($expectedStatus, $this->tester->getStatusCode());
    }

    protected function setUp()
    {
        $this->container = ContainerBuilder::build();
    }

    protected function tearDown()
    {
        $this->tester    = null;
        $this->container = null;
    }
}
