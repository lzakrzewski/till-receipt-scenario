<?php

declare(strict_types=1);

namespace tests\integration\TillReceipt\CLI\Input;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use tests\integration\IntegrationTestCase;

abstract class CollectInputTestCase extends IntegrationTestCase
{
    protected function input(array $inputs): InputInterface
    {
        $stream = fopen('php://memory', 'r+', false);

        fputs($stream, implode(PHP_EOL, $inputs));
        rewind($stream);

        $input = new ArrayInput([]);
        $input->setStream($stream);

        return $input;
    }

    protected function output(): OutputInterface
    {
        return new NullOutput();
    }
}
