<?php

declare(strict_types=1);

namespace tests\integration\TillReceipt\CLI\Input;

use TillReceipt\CLI\Input\CollectsDiscount;
use TillReceipt\Model\Price;

class CollectsDiscountTest extends CollectInputTestCase
{
    /** @var CollectsDiscount */
    private $collectsDiscount;

    /** @test */
    public function it_can_collects_a_discount_from_input()
    {
        $input = $this->input(['Yes', '12.01', '$']);

        $discount = $this->collectsDiscount->collect($input, $this->output());

        $this->assertEquals(new Price(1201, '$'), $discount);
    }

    /** @test */
    public function it_returns_null_when_no_discount()
    {
        $input = $this->input(['N']);

        $discount = $this->collectsDiscount->collect($input, $this->output());

        $this->assertNull($discount);
    }

    /** @test */
    public function it_can_not_collects_discount_from_input_when_input_is_incomplete()
    {
        $this->expectException(\RuntimeException::class);

        $input = $this->input([]);

        $this->collectsDiscount->collect($input, $this->output());
    }

    /** @test */
    public function it_can_not_collects_discount_from_input_when_input_is_invalid()
    {
        $this->expectException(\InvalidArgumentException::class);

        $input = $this->input(['Y', 'xxxx', '$']);

        $this->collectsDiscount->collect($input, $this->output());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->collectsDiscount = $this->container()[CollectsDiscount::class];
    }

    protected function tearDown()
    {
        $this->collectsDiscount = null;

        parent::tearDown();
    }
}
