<?php

declare(strict_types=1);

namespace tests\integration\TillReceipt\CLI\Input;

use TillReceipt\CLI\Input\CollectsProducts;
use TillReceipt\Model\Price;
use TillReceipt\Model\Product;

class CollectsProductsTest extends CollectInputTestCase
{
    /** @var CollectsProducts */
    private $collectsProducts;

    /** @test */
    public function it_can_collects_a_few_products_from_input()
    {
        $input = $this->input(['Teapot', '12', '$', 'Yes', 'Iron', '23', '$', 'N']);

        $products = $this->collectsProducts->collect($input, $this->output());

        $this->assertEquals([
            Product::enter('Teapot', new Price(1200, '$')),
            Product::enter('Iron', new Price(2300, '$')),
        ], $products);
    }

    /** @test */
    public function it_can_collects_1_product_from_input()
    {
        $input = $this->input(['Teapot', '12', '$', 'No']);

        $products = $this->collectsProducts->collect($input, $this->output());

        $this->assertEquals([
            Product::enter('Teapot', new Price(1200, '$')),
        ], $products);
    }

    /** @test */
    public function it_can_not_collects_products_from_input_when_input_is_incomplete()
    {
        $this->expectException(\RuntimeException::class);

        $input = $this->input(['Teapot', '12']);

        $this->collectsProducts->collect($input, $this->output());
    }

    /** @test */
    public function it_can_not_collects_products_from_input_when_input_is_invalid()
    {
        $this->expectException(\InvalidArgumentException::class);

        $input = $this->input(['Teapot', 'xxx', '$', 'N']);

        $this->collectsProducts->collect($input, $this->output());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->collectsProducts = $this->container()[CollectsProducts::class];
    }

    protected function tearDown()
    {
        $this->collectsProducts = null;

        parent::tearDown();
    }
}
