<?php

declare(strict_types=1);

namespace tests\integration\TillReceipt\CLI\Output;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\StreamOutput;
use tests\integration\IntegrationTestCase;
use TillReceipt\CLI\Output\DisplayAllProducts;
use TillReceipt\Model\Price;
use TillReceipt\Model\Product;
use TillReceipt\Model\TillReceipt;

class DisplayAllProductsTest extends IntegrationTestCase
{
    /** @var resource */
    private $stream;

    /** @var string */
    private $display;

    /** @var DisplayAllProducts */
    private $displayAllProducts;

    /** @test */
    public function it_can_display_all_products()
    {
        $products = [
            Product::enter('Teapot', new Price(10001, 'USD')),
            Product::enter('Iron', new Price(20002, 'USD')),
        ];

        $tillReceipt = TillReceipt::calculate($products);

        $this->displayAllProducts->display($tillReceipt, $this->output());

        $this->assertThatDisplayContains('Teapot');
        $this->assertThatDisplayContains('100.01 USD');
        $this->assertThatDisplayContains('Iron');
        $this->assertThatDisplayContains('200.02 USD');
    }

    protected function setUp()
    {
        parent::setUp();

        $this->stream             = fopen('php://memory', 'w', false);
        $this->displayAllProducts = $this->container()[DisplayAllProducts::class];
    }

    protected function tearDown()
    {
        $this->displayAllProducts = null;
        $this->stream             = null;

        parent::tearDown();
    }

    private function assertThatDisplayContains($expectedString)
    {
        $this->assertContains($expectedString, $this->display());
    }

    private function output(): OutputInterface
    {
        return new StreamOutput($this->stream);
    }

    private function display(): string
    {
        if (null !== $this->display) {
            return $this->display;
        }

        rewind($this->stream);
        $this->display = stream_get_contents($this->stream);

        return $this->display;
    }
}
