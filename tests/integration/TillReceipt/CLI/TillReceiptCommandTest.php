<?php

declare(strict_types=1);

namespace tests\tests\integration\TillReceipt\CLI;

use tests\integration\IntegrationTestCase;
use TillReceipt\CLI\TillReceiptCommand;

class TillReceiptCommandTest extends IntegrationTestCase
{
    /** @var TillReceiptCommand */
    private $cli;

    /** @test */
    public function it_returns_0_exit_code_when_1_product_was_entered()
    {
        $this->executeCLI($this->cli, ['Teapot', '12', '$', 'No', 'N']);

        $this->assertExitCode(0);
    }

    /** @test */
    public function it_fails_when_incomplete_input_was_entered()
    {
        $this->expectException(\RuntimeException::class);

        $this->executeCLI($this->cli, ['Teapot']);
    }

    /** @test */
    public function it_returns_1_exit_code_when_invalid_input_was_entered()
    {
        $this->executeCLI(
            $this->cli,
            ['Teapot', 'xxx', '$', 'Yes', 'Iron', '22', '$', 'No', 'N']
        );

        $this->assertExitCode(1);
    }

    /** @test */
    public function it_returns_0_exit_code_when_more_than_1_product_was_entered()
    {
        $this->executeCLI(
            $this->cli,
            ['Teapot', '12', '$', 'Yop', 'Iron', '22', '$', 'N', 'N']
        );

        $this->assertExitCode(0);
    }

    /** @test */
    public function it_returns_0_exit_code_when_discount_was_applied()
    {
        $this->executeCLI($this->cli, ['Teapot', '12', '$', 'Nein', 'Oui', '10', '$']);

        $this->assertExitCode(0);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->cli = $this->container()[TillReceiptCommand::class];
    }

    protected function tearDown()
    {
        $this->cli = null;

        parent::tearDown();
    }
}
