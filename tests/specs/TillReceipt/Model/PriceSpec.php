<?php

declare(strict_types=1);

namespace tests\specs\TillReceipt\Model;

use PhpSpec\ObjectBehavior;
use TillReceipt\Model\Price;

/** @mixin Price */
class PriceSpec extends ObjectBehavior
{
    public function it_can_be_constructed()
    {
        $this->beConstructedWith(100, 'Druggat');

        $this->amount()->shouldBe(100);
        $this->currency()->shouldBe('Druggat');
    }

    public function it_has_string_representation()
    {
        $this->beConstructedWith(100001, 'Druggat');

        $this->__toString()->shouldBe('1000.01 Druggat');
    }

    public function it_can_be_added()
    {
        $this->beConstructedWith(100, 'Druggat');

        $copy = $this->add(new Price(101, 'Druggat'));
        $copy = $copy->add(new Price(1, 'Druggat'));

        $copy->amount()->shouldBe(202);
        $copy->currency()->shouldBe('Druggat');
    }

    public function it_can_not_be_added_with_different_currency()
    {
        $this->beConstructedWith(100, 'Druggat');

        $this->shouldThrow(\InvalidArgumentException::class)->during('add', [new Price(101, 'Different')]);
    }

    public function it_can_be_subtracted()
    {
        $this->beConstructedWith(100, 'Druggat');

        $copy = $this->subtract(new Price(11, 'Druggat'));
        $copy = $copy->subtract(new Price(1, 'Druggat'));

        $copy->amount()->shouldBe(88);
        $copy->currency()->shouldBe('Druggat');
    }

    public function it_can_not_be_subtracted_with_different_currency()
    {
        $this->beConstructedWith(100, 'Druggat');

        $this->shouldThrow(\InvalidArgumentException::class)->during('subtract', [new Price(11, 'Different')]);
    }

    public function it_can_not_be_constructed_with_negative_amount()
    {
        $this->beConstructedWith(-100, 'Druggat');

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_can_not_be_constructed_with_empty_currency()
    {
        $this->beConstructedWith(100, '');

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_can_be_constructed_from_string_values()
    {
        $this->beConstructedThrough('fromStringValues', ['100', 'Druggat']);

        $this->amount()->shouldBe(10000);
        $this->currency()->shouldBe('Druggat');
    }

    public function it_can_be_constructed_from_string_values_with_whitespaces()
    {
        $this->beConstructedThrough('fromStringValues', [' 100 ', ' Druggat ']);

        $this->amount()->shouldBe(10000);
        $this->currency()->shouldBe('Druggat');
    }

    public function it_can_be_constructed_from_string_values_with_decimal_amount()
    {
        $this->beConstructedThrough('fromStringValues', ['100.01', 'Druggat']);

        $this->amount()->shouldBe(10001);
        $this->currency()->shouldBe('Druggat');
    }

    public function it_can_be_constructed_from_string_values_with_thousand_separator()
    {
        $this->beConstructedThrough('fromStringValues', ['1,000.01', 'Druggat']);

        $this->amount()->shouldBe(100001);
        $this->currency()->shouldBe('Druggat');
    }

    public function it_can_not_be_constructed_from_string_values_with_negative_amount()
    {
        $this->beConstructedThrough('fromStringValues', ['-1,000.01', 'Druggat']);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_can_not_be_constructed_from_string_values_with_non_numeric_amount()
    {
        $this->beConstructedThrough('fromStringValues', ['invalid', 'Druggat']);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
