<?php

declare(strict_types=1);

namespace tests\specs\TillReceipt\Model;

use PhpSpec\ObjectBehavior;
use TillReceipt\Model\Price;
use TillReceipt\Model\Product;

/** @mixin Product */
class ProductSpec extends ObjectBehavior
{
    public function it_can_be_constructed()
    {
        $this->beConstructedThrough('enter', ['Teapot', new Price(100, 'USD')]);

        $this->shouldBeAnInstanceOf(Product::class);
        $this->name()->shouldBe('Teapot');
        $this->price()->shouldBeLike(new Price(100, 'USD'));
    }

    public function it_can_not_be_entered_with_blank_name()
    {
        $this->beConstructedThrough('enter', ['', new Price(100, 'USD')]);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
