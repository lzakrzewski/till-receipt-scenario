<?php

declare(strict_types=1);

namespace tests\specs\TillReceipt\Model;

use PhpSpec\ObjectBehavior;
use TillReceipt\Model\Price;
use TillReceipt\Model\Product;
use TillReceipt\Model\TillReceipt;

/** @mixin TillReceipt */
class TillReceiptSpec extends ObjectBehavior
{
    public function it_can_be_calculated_with_products()
    {
        $products = [
            Product::enter('Teapot', new Price(1001, 'USD')),
            Product::enter('Iron', new Price(1002, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->products()->shouldBeLike(
          [
              Product::enter('Teapot', new Price(1001, 'USD')),
              Product::enter('Iron', new Price(1002, 'USD')),
          ]
        );
    }

    public function it_can_not_be_empty()
    {
        $products = [];

        $this->beConstructedThrough('calculate', [$products]);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_can_not_be_calculated_with_invalid_products()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            new \stdClass(),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_can_not_be_calculated_with_products_in_different_currency()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(1000, 'GBP')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_has_sub_total()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->subTotal()->shouldBeLike(new Price(3200, 'USD'));
    }

    public function it_has_grand_total()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->grandTotal()->shouldBeLike(new Price(3200, 'USD'));
    }

    public function it_can_apply_discount()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->applyDiscount(new Price(100, 'USD'));

        $this->grandTotal()->shouldBeLike(new Price(3100, 'USD'));
    }

    public function it_has_discount()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->applyDiscount(new Price(100, 'USD'));

        $this->discount()->shouldBeLike(new Price(100, 'USD'));
        $this->hasDiscountApplied()->shouldBe(true);
    }

    public function it_has_not_discount()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->discount()->shouldBe(null);
        $this->hasDiscountApplied()->shouldBe(false);
    }

    public function it_can_not_apply_discount_in_different_currency()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->shouldThrow(\InvalidArgumentException::class)->during('applyDiscount', [new Price(100, 'GBP')]);
    }

    public function it_can_not_apply_discount_equal_to_sub_total()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->shouldThrow(\InvalidArgumentException::class)->during('applyDiscount', [new Price(3200, 'GBP')]);
    }

    public function it_can_not_apply_discount_greater_than_subtotal()
    {
        $products = [
            Product::enter('Teapot', new Price(1000, 'USD')),
            Product::enter('Iron', new Price(2200, 'USD')),
        ];

        $this->beConstructedThrough('calculate', [$products]);

        $this->shouldThrow(\InvalidArgumentException::class)->during('applyDiscount', [new Price(3201, 'GBP')]);
    }
}
